const Collectors = require('../models/SubscribeCollectorModel');
const validator = require('validator');

class SubscribeCollectorController {
  // GET FIND ALL
  async findAll(req, res){
    try{
        const collectors = await Collectors.find({});
        res.send(collectors);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add message to DB
  async addCollector(req, res){
    const {firstName:contactName, lastName:contactLastName, email:contactEmail}=req.body
    if (!contactName || !contactEmail) return res.json({ ok: false, messageOutput: 'First name and email are required' });
    if (!validator.isEmail(contactEmail)) return res.json({ ok: false, messageOutput: 'Please provide a valid email' });
    try{
      const user = await Collectors.findOne({ email:contactEmail });
      if (user) return res.json({ ok: false, messageOutput: 'The provided email is already subscribed' });
        Collectors.create({firstName:contactName,lastName:contactLastName, email:contactEmail, type:'collector'})
        res.json({ ok: true, messageOutput: 'Successfully subscribed' });
      }
    catch(e){
      res.json({ ok: false, error });
    }
  }



  // async deleteCollector(req,res){
  //   let {id:messagID}=req.body
  //   let exists= await Messages.exists({_id:messagID})
  //   try{
  //     if(exists){
  //       await Messages.deleteOne({_id:messagID});
  //       res.send(`The message with ID ${messagID} was successfully deleted`)
  //     }else{
  //       res.send(`The message with ID ${messagID} does not exist in the DB and hence cannot be deleted`)
  //     }
  //   }
  //   catch(error){
  //     res.send({error})
  //   }
  // }

  async deleteCollector (req, res) {
    let { email } = req.body
    if (!email) return res.json({ message: 'Enter your email' })
    if (!validator.isEmail(email)) res.json({ message: 'Enter a valid email address' })
    try {
      const subscriber = await Collectors.findOne({
        email: email
      })
      if (!subscriber) return res.json({ok: false, message: 'This email is not used for subscription'})
      const unsubscriber = await Collectors.deleteOne({
        email: email
      })
      res.json({ ok: true, message: 'You are successfully unsubscribed' })
    }
    catch (error) {
      console.log('error====>', error)
      res.send({ ok: false, error })
    }
  }

};

module.exports = new SubscribeCollectorController();