const Collectors = require('../models/SubscribeCollectorModel');
const Gallerists = require('../models/SubscribeGalleristModel');
const Artists = require('../models/SubscribeArtistModel');
const validator = require('validator');

class UnsubcribeUserController {
  async deleteUser (req, res) {
    let { email } = req.body
    if (!email) return res.json({ message: 'Enter your email' })
    if (!validator.isEmail(email)) res.json({ message: 'Enter a valid email address' })
    try {
      const collector = await Collectors.findOne({
        email: email
      })
      const gallerist = await Gallerists.findOne({
        email: email
      })
      const artist = await Artists.findOne({
        email: email
      })
      if (!collector&&!gallerist&&!artist) return res.json({ok: false, message: 'This email is not used for subscription'})
      if(collector){
        await Collectors.deleteOne({
          email: email
        })
      }
      if(gallerist){
        await Gallerists.deleteOne({
          email: email
        })
      }
      if(artist){
        await Artists.deleteOne({
          email: email
        })
      }
      res.json({ ok: true, message: 'You are successfully unsubscribed' })
    }
    catch (error) {
      console.log('error====>', error)
      res.send({ ok: false, error })
    }
  }

};

module.exports = new UnsubcribeUserController();