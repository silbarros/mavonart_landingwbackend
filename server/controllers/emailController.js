const nodemailer = require('nodemailer')
const transport = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: process.env.NODEMAILER_EMAIL,
    pass: process.env.NODEMAILER_PASSWORD,
  }
});

const welcomeMail = async (req, res) => {
  const { email, name } = req.body
  const mailContent = {
    to: email,
    subject: 'Thank you for subscribing',
    // text: 'You can unsubscribe from http://localhost:3000/unsubscribe',
    html: `<body>
    <p>Hello ${name}</p>
    <h1>Welcome to Mavon</h1>
    <p>You can unsubscribe from <a href="http://localhost:3000/unsubscribe">here</a></p>
    </body>`,
  }
  console.log('mailContent===>', mailContent)
  try {
    const response = await transport.sendMail(mailContent)
    console.log('======Welcome email sent to new subscriber==========')
    return res.json({ ok: true, message: 'email sent' })
  }
  catch (err) {
    console.log('error===>', error)
    return res.json({ ok: false, message: err })
  }
}

const newUserHasSubscribed = async (req, res) => {
  const { email, name, type } = req.body
  const mailContent = {
    to: 'al20070613@gmail.com', //change this to admin email
    subject: 'New user has subscribed',
    html: `<body>
    <p>Name: ${name}</p>
    <p>Type: ${type}</p>
    <p>email: ${email}</p>
    </body>`,
  }
  console.log('mailContent===>', mailContent)
  try {
    const response = await transport.sendMail(mailContent)
    console.log('=====Mail to admin sent that new user has subscripted=======')
    return res.json({ ok: true, message: 'email sent' })
  }
  catch (err) {
    console.log('error===>', error)
    return res.json({ ok: false, message: err })
  }
}

module.exports = { welcomeMail, newUserHasSubscribed }