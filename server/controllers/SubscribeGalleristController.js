const Gallerists = require('../models/SubscribeGalleristModel');
const validator = require('validator');
const sendEmail = require('./email.send')
const templates = require('./email.template')

class SubscribeGalleristController {
  // GET FIND ALL
  async findAll(req, res){
    try{
        const gallerists = await Gallerists.find({});
        res.send(gallerists);
    }
    catch(e){
        res.send({e})
    }
  }

  // Add message to DB
  async addGallerist(req, res){
    const {name:contactName, email:contactEmail}=req.body
    if (!contactName || !contactEmail) return res.json({ ok: false, messageOutput: 'Name and email are required' });
    if (!validator.isEmail(contactEmail)) return res.json({ ok: false, messageOutput: 'Please provide a valid email' });
    try{
      const user = await Gallerists.findOne({ email:contactEmail });
      if (user) return res.json({ ok: false, messageOutput: 'The provided email is already subscribed' });
      Gallerists.create({name:contactName, email:contactEmail, type:'gallerist'})
      .then(newUser => sendEmail(contactName.contactEmail, templates.confirm(newUser)))
        res.json({ ok: true, messageOutput: 'Successfully subscribed' });
      }
    catch(e){
      res.json({ ok: false, error });
    }
  }

  async deleteGallerist (req, res) {
    let { email } = req.body
    if (!email) return res.json({ message: 'Enter your email' })
    if (!validator.isEmail(email)) res.json({ message: 'Enter a valid email address' })
    try {
      const subscriber = await Gallerists.findOne({
        email: email
      })
      if (!subscriber) return res.json({ok: false, message: 'This email is not used for subscription'})
      const unsubscriber = await Gallerists.deleteOne({
        email: email
      })
      res.json({ ok: true, message: 'You are successfully unsubscribed' })
    }
    catch (error) {
      console.log('error====>', error)
      res.send({ ok: false, error })
    }
  }

};

module.exports = new SubscribeGalleristController();