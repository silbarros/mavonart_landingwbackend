const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const subscribeCollectorSchema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: false },
    email: { type: String, unique: false, required: true },
    type: { type: String},
})
module.exports =  mongoose.model('subscribecollector', subscribeCollectorSchema);