const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const subscribeGalleristSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, unique: false, required: true },
    type: { type: String},
})
module.exports =  mongoose.model('subscribegallerist', subscribeGalleristSchema);