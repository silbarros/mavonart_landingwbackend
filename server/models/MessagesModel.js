const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const messagesSchema = new Schema({
    name: { type: String, required: true },
    phone: { type: String, required: false },
    email: { type: String, unique: false, required: true },
    message: { type: String, required: true }
})
module.exports =  mongoose.model('messages', messagesSchema);