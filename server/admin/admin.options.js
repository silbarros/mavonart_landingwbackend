const AdminBro = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');
const Messages = require('../models/MessagesModel');
const SubscribeCollector = require('../models/SubscribeCollectorModel');
const SubscribeGallerist = require('../models/SubscribeGalleristModel');
const SubscribeArtist = require('../models/SubscribeArtistModel')
// const UsersAdmin = require('./resource_options/users.admin');

AdminBro.registerAdapter(AdminBroMongoose);

const options = {
	resources: [ Messages, SubscribeCollector, SubscribeGallerist, SubscribeArtist ] 
};

module.exports = options;
