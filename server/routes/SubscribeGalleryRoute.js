const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/SubscribeGalleryController');

//ADMIN
// see all messages
router.get('/', controller.findAll);

// // == Route to add Message 
router.post('/add', controller.addBuyer);

// // == Route to delete Message 
router.post('/delete', controller.deleteBuyer);





module.exports = router;