const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/SubscribeArtistController');

//ADMIN
// see all messages
router.get('/', controller.findAll);

// // == Route to add Message 
router.post('/add', controller.addArtist);

// // == Route to delete Message 
router.post('/delete', controller.deleteArtist);





module.exports = router;