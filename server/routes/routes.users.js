const express = require('express');
const router = express.Router();
const controller = require('../controllers/controllers.users');

router.post('/register', controller.register);

module.exports = router;