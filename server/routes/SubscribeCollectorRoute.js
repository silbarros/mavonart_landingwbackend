const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/SubscribeCollectorController');

//ADMIN
// see all messages
router.get('/', controller.findAll);

// // == Route to add Message 
router.post('/add', controller.addCollector);

// // == Route to delete Message 
router.post('/delete', controller.deleteCollector);





module.exports = router;