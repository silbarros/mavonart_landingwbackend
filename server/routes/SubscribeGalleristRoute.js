const express     = require('express'), 
    router        = express.Router(),
    controller    = require('../controllers/SubscribeGalleristController');

//ADMIN
// see all messages
router.get('/', controller.findAll);

// // == Route to add Message 
router.post('/add', controller.addGallerist);

// // == Route to delete Message 
router.post('/delete', controller.deleteGallerist);





module.exports = router;