const router     = require('express').Router();
const controller = require('../controllers/emailController.js')

router.post('/welcomeMail',controller.welcomeMail)
router.post('/newUserHasSubscribed',controller.newUserHasSubscribed)

module.exports = router