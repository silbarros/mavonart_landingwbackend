const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    messagesRoute = require('./routes/MessagesRoute'),
    subscribeCollectorRoute = require('./routes/SubscribeCollectorRoute'),
    subscribeGalleristRoute = require('./routes/SubscribeGalleristRoute'),
    subscribeArtistRoute = require('./routes/SubscribeArtistRoute'),
    unsubscribeUserRoute = require('./routes/UnsubscribeUserRoute'),
    bodyParser = require('body-parser');
    require('dotenv').config();
// ===================== Admin Bro ===================
const AdminBro = require('admin-bro');
const options = require('./admin/admin.options');
const buildAdminRouter = require('./admin/admin.router');
const admin = new AdminBro(options);
const router = buildAdminRouter(admin);
const https = require('https')
const fs = require('fs')

Certificates
const privateKey = fs.readFileSync('/etc/letsencrypt/live/mavon.art/privkey.pem','utf8')
const certificate = fs.readFileSync('/etc/letsencrypt/live/mavon.art/cert.pem','utf8')
const ca = fs.readFileSync('/etc/letsencrypt/live/mavon.art/chain.pem','utf8')

const credentials={
  key:privateKey,
  cert:certificate,
  ca:ca
}

const https_server = https.createServer(credentials, app)

app.use(admin.options.rootPath, router);

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo
const cors = require('cors');
const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));


 
// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb+srv://mavonuser:mavonArt2021@cluster0.mlexf.mongodb.net/database?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

// routes
app.use('/messages', messagesRoute);
app.use('/subscribe-collector', subscribeCollectorRoute);
app.use('/subscribe-gallerist', subscribeGalleristRoute);
app.use('/subscribe-artist', subscribeArtistRoute);
app.use('/unsubscribe-user', unsubscribeUserRoute);
app.use('/users', require('./routes/routes.users'));
app.use('/email', require('./routes/emailRoute'))


// Set the server to listen on port 3001
// app.listen(3001, () => console.log(`listening on port 3001`))
https_server.listen('8443', ()=>console.log('https erver running on port 8443 '))