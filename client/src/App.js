import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet"
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import './App.css';
import Header from "./pages/Header";
import Home from './pages/Home'
import Subscribe from './pages/Subscribe';
import Footer from './pages/Footer';
import Contact from './pages/Contact';
// import Gallery from './pages/Gallery'
import ScrollToTop from './components/scrollToTop';
// import ArtPiece from './pages/ArtPiece';
import WhoWeServe from "./pages/WhoWeServe";
import Medium from './pages/Medium'
import Unsubscribe from "./pages/Unsubscribe";

import { render } from 'react-dom'
const rootNode = document.getElementById('root')

function App() {

  // const isLandscape = () => window.matchMedia('(orientation:landscape)').matches,
  //   [orientation, setOrientation] = useState(isLandscape() ? 'landscape' : 'portrait'),
  //   onWindowResize = () => {
  //     clearTimeout(window.resizeLag)
  //     window.resizeLag = setTimeout(() => {
  //       delete window.resizeLag
  //       setOrientation(isLandscape() ? 'landscape' : 'portrait')
  //     }, 200)
  //   }

  // useEffect(() => (
  //   onWindowResize(),
  //   window.addEventListener('resize', onWindowResize),
  //   () => window.removeEventListener('resize', onWindowResize)
  // ), [])

  const isPortrait = () => window.matchMedia('(orientation:portrait)').matches,
  [orientation, setOrientation] = useState(isPortrait() ? 'portrait' : 'landscape'),
  onWindowResize = () => {
    clearTimeout(window.resizeLag)
    window.resizeLag = setTimeout(() => {
      delete window.resizeLag
      setOrientation(isPortrait() ? 'portrait' : 'landscape')
    }, 200)
  }

useEffect(() => (
  onWindowResize(),
  window.addEventListener('resize', onWindowResize),
  () => window.removeEventListener('resize', onWindowResize)
), [])

 

return (
  <div className="App">
  {/* <div>{orientation}</div> */}
    <Router>
      <ScrollToTop />

      <Route render={props => <Header {...props} />} />


      <Route exact path='/contact-us' component={Contact} />

      <Route exact path='/' component={Home} />
      <Route exact path='/#philosophy' component={Home} />

      {/* <Route exact path='/' component={About}/> */}

      {/* <Route exact path='/' component={Mission}/> */}

      <Route exact path='/' component={Subscribe} />

      <Route exact path='/unsubscribe' component={Unsubscribe} />

      {/* <Route exact path='/gallery' component={Gallery} /> */}

      {/* <Route exact path='/gallery/art-piece' component={ArtPiece} /> */}

      <Route exact path='/who-we-serve' component={WhoWeServe} />

      <Route exact path='/medium' component={Medium} />

      <Footer />

    </Router>

  </div>
);
}

render (
  <App />,
  rootNode
)

export default App;
