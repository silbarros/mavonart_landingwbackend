import React, { useState } from "react";
import axios from 'axios';
import { URL } from '../config';
import '../App.css';
import '../css/Subscribe.css';
import '../css/Buttons.css';

function App() {
  const [whoIsSubscribing, setWhoIsSubscribing] = useState('collector')

  const [formCollector, setValuesCol] = useState({
    name: '',
    lastName: '',
    email: '',
  });
  console.log('collectorname===>', formCollector.name)
  console.log('collectorlastname===>', formCollector.lastName)
  console.log('collectoremail===>', formCollector.email)

  const [formGallerist, setValuesGal] = useState({
    name: '',
    email: '',
  });


  const [formArtist, setValuesArt] = useState({
    name: '',
    lastName: '',
    email: '',
  });
  console.log('artistname===>', formArtist.name)
  console.log('artistlastname===>', formArtist.lastName)
  console.log('artistemail===>', formArtist.email)

  const [message, setMessage] = useState('')

  const handleChangeCollector = (evt) => {
    setValuesCol({ ...formCollector, [evt.target.name]: evt.target.value });
  };

  const handleChangeGallerist = (evt) => {
    setValuesGal({ ...formGallerist, [evt.target.name]: evt.target.value });
  };

  const handleChangeArtist = (evt) => {
    setValuesArt({ ...formArtist, [evt.target.name]: evt.target.value });
  };

  const handleSubmitCollector = async (evt) => {
    evt.preventDefault();
    try {
      const response = await axios.post(`${URL}/subscribe-collector/add`, {
        firstName: formCollector.name,
        lastName: formCollector.lastName,
        email: formCollector.email,
      });
      setMessage(response.data.messageOutput);
      // setValuesCol({ [evt.target.value]: '' })
      setTimeout(() => {
        setMessage('')
      }, 3000);
      if (response.data.ok) {
        handleSendWelcomeMail()
        handleSendMailToAdmin()
        evt.target.reset()
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmitGallerist = async (evt) => {
    evt.preventDefault();
    try {
      const response = await axios.post(`${URL}/subscribe-gallerist/add`, {
        name: formGallerist.name,
        email: formGallerist.email,
      });
      setMessage(response.data.messageOutput);
      // setValuesGal({ [evt.target.value]: '' })
      setTimeout(() => {
        setMessage('')
      }, 3000);
      if (response.data.ok) {
        handleSendWelcomeMail()
        handleSendMailToAdmin()
        evt.target.reset()
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmitArtist = async (evt) => {
    evt.preventDefault();
    try {
      const response = await axios.post(`${URL}/subscribe-artist/add`, {
        firstName: formArtist.name,
        lastName: formArtist.lastName,
        email: formArtist.email,
      });
      setMessage(response.data.messageOutput);
      // setValuesArt({ [evt.target.value]: '' })
      setTimeout(() => {
        setMessage('')
      }, 3000);
      if (response.data.ok) {
        handleSendWelcomeMail()
        handleSendMailToAdmin()
        evt.target.reset()
      }
    } catch (error) {
      console.log(error);
    }
  };


  const handleSendWelcomeMail = async () => {
    try {
      if (whoIsSubscribing === 'collector') {
        await axios.post(`${URL}/email/welcomeMail`, {
          email: formCollector.email,
          name: `${formCollector.name} ${formCollector.lastName}`
        })
      }
      if (whoIsSubscribing === 'gallerist') {
        await axios.post(`${URL}/email/welcomeMail`, {
          email: formGallerist.email,
          name: formGallerist.name
        })
      }
      if (whoIsSubscribing === 'artist') {
        await axios.post(`${URL}/email/welcomeMail`, {
          email: formArtist.email,
          name: `${formArtist.name} ${formArtist.lastName}`
        })
      }
    } catch (error) {
      console.log('error===>', error)
    }
  }



  const handleSendMailToAdmin = async () => {
    try {
      if (whoIsSubscribing === 'collector') {
        await axios.post(`${URL}/email/newUserHasSubscribed`, {
          email: formCollector.email,
          name: `${formCollector.name} ${formCollector.lastName}`,
          type: whoIsSubscribing
        })
      }
      if (whoIsSubscribing === 'gallerist') {
        await axios.post(`${URL}/email/newUserHasSubscribed`, {
          email: formGallerist.email,
          name: formGallerist.name,
          type: whoIsSubscribing
        })
      }
      if (whoIsSubscribing === 'artist') {
        await axios.post(`${URL}/email/newUserHasSubscribed`, {
          email: formArtist.email,
          name: `${formArtist.name} ${formArtist.lastName}`,
          type: whoIsSubscribing
        })
      }
    } catch (error) {
      console.log('error===>', error)
    }
  }

  return (
    <div >
      <h2 className='i-am-a'>I am a...</h2>
      
      <label for="collector" class="radio">
      <input type= "radio" 
      name= "whoIsSubscribing"  
      onChange={e => setWhoIsSubscribing('collector')} 
      id= "collector"
      class= "radio__input"
      defaultChecked>
      </input>
       <div class= "radio__radio"></div>
       Collector
       </label> 
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

       <label for="gallerist" class="radio">
      <input type= "radio" 
      name= "whoIsSubscribing"  
      onChange={e => setWhoIsSubscribing('gallerist')} 
      id= "gallerist"
      class= "radio__input">
      </input>
       <div class= "radio__radio"></div>
       Gallerist
       </label> 
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

       <label for="artist" class="radio">
      <input type= "radio" 
      name= "whoIsSubscribing"  
      onChange={e => setWhoIsSubscribing('artist')} 
      id= "artist"
      class= "radio__input">
      </input>
       <div class= "radio__radio"></div>
       Artist
       </label> 
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;




      {whoIsSubscribing === 'collector'
        ? <form className='subscribe-form' onChange={handleChangeCollector} onSubmit={
          handleSubmitCollector}>
          <div className='subscribe-name-wrapper'>
            <input className='subscribe-name  subscribe-first-name' name='name' placeholder='First name' />
            <input className='subscribe-name subscribe-last-name' name='lastName' placeholder='Last name'/>
            <input className='subscribe-email' name='email' placeholder='Your email' />
            <button className='subscribe-button'>Subscribe</button>
            <p className='unsubscribe-note' >You can unsubscribe at anytime following the link in the welcoming e-mail.</p>
          </div>
          <br></br>
          <div className='subscribe-message'>{message}</div>
        </form>
        : whoIsSubscribing === 'gallerist'
          ? <form className='subscribe-form' onChange={handleChangeGallerist} onSubmit={handleSubmitGallerist}>
            <div className='subscribe-gallery-name-wrapper'>
              <input className='subscribe-gallery-name' name='name' placeholder='Gallery name' />
              <input className='subscribe-email' name='email' placeholder='Your email' />
              <button className='subscribe-button'>Subscribe</button>
              <p className='unsubscribe-note'>You can unsubscribe at anytime following the link in the welcoming e-mail.</p>
            </div>
            <br></br>
            <div className='subscribe-message'>{message}</div>
          </form>
          : whoIsSubscribing === 'artist'
          && <form className='subscribe-form' onChange={handleChangeArtist} onSubmit={handleSubmitArtist}>
            <div className='subscribe-name-wrapper'>
              <input className='subscribe-name  subscribe-first-name' name='name' placeholder='First name' />
              <input className='subscribe-name subscribe-last-name' name='lastName' placeholder='Last name' />
              <input className='subscribe-email' name='email' placeholder='Your email' />
              <button className='subscribe-button'>Subscribe</button>
              <p className='unsubscribe-note' >You can unsubscribe at anytime following the link in the welcoming e-mail.</p>
            </div>
            <br></br>
            <div className='subscribe-message' >{message}</div>

          </form>
      }




    </div>
  );
}

export default App;
