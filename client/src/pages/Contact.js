import React, {useState} from "react";
import {Helmet} from "react-helmet"
import axios from 'axios';
import { URL } from '../config';
import '../App.css';

const Contact = () => {

	const [ form, setValues ] = useState({
		name: '',
    phone:'',
		email: '',
		message: ''
	});
  const [message,setMessage] = useState('')


  const handleChange = (evt) => {
		setValues({ ...form, [evt.target.name]: evt.target.value });
	};

  const handleSubmit = async(evt) => {
		evt.preventDefault();
		try {
			const response = await axios.post(`${URL}/messages/add`, {
				name: form.name,
				phone: form.phone,
				email: form.email,
				message: form.message
			});
			setMessage(response.data.messageOutput);
      setValues({[evt.target.value]:''})
      setTimeout(() => {
        setMessage('')
      }, 1000);
      evt.target.reset()
		} catch (error) {
			console.log(error);
		}

	};

  return(
  <div className="contact-us">
      <Helmet>
        <html lang="en" />
        <title>Mavon | Contact us</title>
        <meta name="description" content="Let's talk. We would love to learn about your project." />
        {/* <meta name="robots" content="nosnippet"/> */}
      </Helmet>
    <div className='contact-content'>
      <h2 className='contact-title'>Let's talk</h2>
      <p>We would love to learn about your project.</p>      
      <form className='contact-form' onChange={handleChange} onSubmit={handleSubmit}>
        <div className='contact-wrapper'>
          <input placeholder='Name' name='name'/>
          <input placeholder='Phone' name='phone'/>
          <input placeholder='Your email' name='email' className='contact-email'/>
          <textarea placeholder='Type your message here...' name='message' className='contact-message' name="message" rows="10"></textarea>  
          {/* <input placeholder='Type your message here...' name='message' className='contact-message'/> */}
        </div>
          <button className='contact-button'>Send</button>
        <div className='contact-message'>{message}</div>
      </form>
    </div> 
  </div>
  )
};

export default Contact;