import React from 'react'
import Carousel from './Carousel'
import CarouselArtists from './CarouselArtists'
import '../App.css'

const Gallery1 = () => {

  return (
    <div className='grid-two-columns section' id='home'>
      
      <div className='home-left'>
        <h1 className='home-title'>Gallery 101, Berlin</h1>
        <p className='home-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <button className='get-started-button'>See Gallery</button>
      </div>

      <div>
        <img src='https://picsum.photos/288/376' className='home-image' style={{marginRight: 24}}/>
      </div>

      {/* <CarouselArtists/> <br></br> */}

      <div className='art-piece-carrousel'>
        <h1 className='home-title'>Art Piece</h1>
        <Carousel show={4}>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        </Carousel> 
      </div>


    </div>
    
  )
}

export default Gallery1
