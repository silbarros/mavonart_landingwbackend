import React, { useState, useEffect } from 'react';
import CarouselArtistsContent from './CarouselArtistsContent'

const CarouselArtists = (props) => {
  
  return (
      <div className="carousel-artists-container">
       <div className='artists-carrousel'>
        <h1 className='home-title'>Artists</h1>
        <CarouselArtistsContent show={4}>
        <div>
          <div className='carousel-artists-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
            <img src="https://picsum.photos/288/376" alt="placeholder"/>
          </div>
        </div>
        </CarouselArtistsContent> 
      </div>
      </div>
  )
}

export default CarouselArtists