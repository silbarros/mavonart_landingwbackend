import React, { useState, useLayoutEffect } from 'react'
import {NavLink} from 'react-router-dom'
import Carousel from './Carousel'


const Gallery = () => {
    var numberOfImagesArtist = 4;
    var numberOfImagesArtPieces = 3;
    const [width, height] = useWindowSize();

  function useWindowSize() {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
      function updateSize() {
        setSize([window.innerWidth, window.innerHeight]);
      }
      window.addEventListener('resize', updateSize);
      updateSize();
      
      return () => window.removeEventListener('resize', updateSize);

    }, []);
    return size;
  }

    if(width<=750){
      numberOfImagesArtist=1;
      numberOfImagesArtPieces = 1;
    }

  return <div>
    <div className='section grid-two-columns'>
      <div className='gallery-description'/*style={{ marginLeft: 109 }}*/>
        <p style={{ fontSize: 12, fontFamily: 'Poppins', fontWeight: 400 }}><NavLink exact to={'/'} className='button-back-home'>Home</NavLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Galleries&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;＞&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gallery 101</p>

        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', marginTop: 38.5, marginBottom: 0 }}>Gallery 101, Berlin</h1>

        <p style={{ fontSize: 18, fontFamily: 'Avenir', fontWeight: 400, color: '#404040', maxWidth: 520, marginTop: 47, marginBottom: 0 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <button className='get-started-button' style={{ marginTop: 26, }}>See Gallery</button>
      </div>

      <div>
        <img className='gallery-description-image' src='https://picsum.photos/526/687' style={{ borderRadius: 8 }} />
      </div>
    </div>


    <div className='section section-artists'>
      <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1}} />
      <div className='artists-title'/*style={{ marginLeft: 109 }}*/>
        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', paddingTop: 71, marginTop: 0, marginBottom: 0 }}>Artists</h1>
      </div>
    
      <div className='carousel-wrapper'>
      <Carousel show={numberOfImagesArtist}>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>
          </div>
        </div>
        <div>
          <div className='carousel-artists-img'>
            <img src='https://picsum.photos/338/338' />
            <p>artist name</p>   
          </div>
        </div>
        </Carousel> 
        </div>
    </div>



    <div className='section section-artpiece'>
      <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1}} />
      <div className='section-artpiece-title'/*style={{ marginLeft: 109 }}*/>
        <h1 style={{ fontSize: 45, fontFamily: 'Playfair Display', fontWeight: 400, color: '#22282B', paddingTop: 71, marginTop: 0, marginBottom: 0}}>Art Piece</h1>
      </div>
      <Carousel show={numberOfImagesArtPieces}>
        <div>
          <div className='carousel-artpiece-img'>
            <NavLink exact to={'/gallery/art-piece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}
          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/art-piece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/art-piece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/art-piece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
        <div>
          <div className='carousel-artpiece-img'>
          <NavLink exact to={'/gallery/art-piece'}><img src='https://picsum.photos/511/511'/>  </NavLink>
            {/* <img src='https://picsum.photos/511/511'/>   */}          </div>
        </div>
      </Carousel> 

    </div>

  </div>
}

export default Gallery