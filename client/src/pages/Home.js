import React, { useEffect, useState } from 'react';
import {NavLink} from 'react-router-dom'
import {Helmet} from "react-helmet"
import philosophyDefinition from '../images/philosophy_big.png'
import philosophyDefinitionMob from '../images/philosophy_mobile.png'
import topimg from '../images/top.png'
import mediumimg from '../images/medium.png'
import whoweserveimg from '../images/whoweserve.jpg'
import '../App.css'

const Home = () => {

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const imageUrl = windowWidth >= 450 ? philosophyDefinition : philosophyDefinitionMob;

  useEffect(() => {
      const handleWindowResize = () => {
          setWindowWidth(window.innerWidth);
      };
      
      window.addEventListener('resize', handleWindowResize);

      return () => {
          window.removeEventListener('resize', handleWindowResize);
      }
  }, []);

  return (
    <div>
      <Helmet>
        <html lang="en" />
        <title>Mavon | Create, Connect, Collect.</title>
        <meta name="description" content="Join our unique Digital Marketplace for non-fungible tokens (NFTs) to create your own private rooms and exhibit exclusive digital assets." />
        {/* <meta name="robots" content="nosnippet"/> */}
      </Helmet>

      <div className='grid-two-columns section' name='home'>
        {/* <div className='home-left'> */}
        <div className='home-section'>
          <h1 className='home-title'>Create, Connect, Collect.</h1>
          <p className='home-description'>Coming this Autumn/Winter... 
          <br></br> <br></br> <br></br>
          Mavon creates a platform-based tool for galleries to promote digital artworks (NFTs) directly to a collecting audience.</p>
          {/* <button className='get-started-button'>Get started</button> */}
        </div>

        <div className='home-images-wrapper'>
          <img src={topimg} className='home-image home-image1' style={{ marginRight: 24 }} />
          {/* <img src='https://picsum.photos/288/376' className='home-image home-image2' /> */}
        </div>
      </div>

      <hr className="hr-home" style={{ backgroundColor: "#E5E5E5", border: "none", height: 1 }} id='philosophy' />

      
      <div className='grid-two-columns section section-mission section-philosophy'>
        {/* <img src={philosophyDefinition} srcSet={`${philosophyDefinitionMob} 300w, ${philosophyDefinition} 600w`} className='about-image mission-image philoshopy-image' /> */}
        <img src={imageUrl} className='about-image mission-image philoshopy-image'/>
        <div className='mission-description-wrapper philosophy-description-wrapper' style={{height:420}}>
          <h1 className='philosophy-title' >Our Philosophy</h1>
            <p className='mission-description philosophy-description'> Art purveys powerful messages, moves people deeply, and captures impactful moments in one's life - whether it might be a painting, an installation, or a digital artwork. 
            We recognise a trend that we head towards a future of duality between physical and digital in every aspect of our lives. 
            
            The gallerist as curator and expert has the sharpened view and turns the enthusiast's eye towards subjects and objects that are of matter. We make it our duty to extend and preserve that function into a digitised environment. 
            
            Together, we continue to challenge the power of expression and pave a path for those who are rich in it. </p>
        </div>
      </div>


      <hr className="hr-philosophy" style={{ backgroundColor: "#E5E5E5", border: "none", height: 1 }} />

      <div className='grid-two-columns section section-who-we-serve'>
        <div>
          <h1 className='about-title'>Who We Serve</h1>
          <div>

          </div>
          <p className='about-description'>We identify our mission as being the supporting act and for you as <b>gallerist</b>, <b>artist</b>, or <b>collector</b>. Our pride strives from fulfilling the duty to let you shine. In order to do so, we listen to your needs to create a marvelous, tailored experience. Your way is the right way. 
          Here, is where our journey begins. </p>
           <NavLink exact to={'/who-we-serve'} className='read-more-button'>Read More →</NavLink>
        </div>

        <img src={whoweserveimg} className='about-image who-we-serve-image' />
      </div>


      <hr className="hr-who-we-serve" style={{ backgroundColor: "#E5E5E5", border: "none", height: 1 }} />

      <div className='grid-two-columns section section-mission section-medium'>
        <img src={mediumimg} className='about-image mission-image medium-image' />
        <div className='mission-description-wrapper medium-description-wrapper'>
          <h1 className='mission-title'>The Medium</h1>
          <p className='mission-description medium-description'>Digital art is a new form of creating art based on the concept of non-fungible tokens (NFT). It refers to a token, a digital asset, which is not fungible, thus authentic and unique. The technology is based on various blockchains that solve forgery issues and transparency in a piece's lifeline.</p>
          <NavLink exact to={'/medium'} className='read-more-button'>Read More →</NavLink>
        </div>
      </div>

      <hr className="hr-medium" style={{ backgroundColor: "#E5E5E5", border: "none", height: 1 }} />

    </div>
  )
}

export default Home
