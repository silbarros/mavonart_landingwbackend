import React from 'react';
import {NavLink} from 'react-router-dom';

const Mission = () => {

  return (
    <div className='grid-two-columns section section-mission' id='mission'>
      <img src='https://picsum.photos/600/379' className='about-image mission-image' />
      <div className='mission-description-wrapper'>
        <h1 className='mission-title'>Our Mission</h1>
        <p className='mission-description'>Like how blockchain democratizes finance like never before, APENFT, by turning top artists and art pieces into NFTs,<br/><br/>
          not only upgrades the way artworks are hosted, but also transforms them from being elite-exclusive items to something
          that truly belongs to the people and mirrors their aspirations. APENFT is the art for everyone.</p>
        {/* <button className='read-more-button'>Read More →</button> */}
        <NavLink exact to={'/who-we-serve'} className='read-more-button'>Read More →</NavLink>
      </div>
    </div>
  )
}

export default Mission