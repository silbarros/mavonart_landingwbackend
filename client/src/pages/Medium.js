import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import {Helmet} from "react-helmet"
import '../css/Medium.css'

const Medium = (props) => {

  return (
    <div className='medium' id='medium'>
      <Helmet>
        <html lang="en" />
        <title>Mavon | The Medium</title>
        <meta name="description" content="Digital art is a new form of creating art based on the concept of non-fungible tokens (NFT). It refers to a token, a digital asset, which is not fungible, thus authentic and unique." />
        {/* <meta name="robots" content="nosnippet"/> */}
      </Helmet>
      <p className='medium-button'><NavLink exact to={'/'}>←Go Back Home</NavLink></p>

      {/* <span className='button-back-home' onClick={() => props.history.push('/')}><p className='medium-button'>←Go Back Home</p></span> */}
      <h1 className='medium-title'>The Medium</h1>
      <p className='medium-subtitle'> “The sculpture is just as far away from plastic painting as the digital artwork.”</p>

      <div className='medium-columns'>
        <div>
          <p>Digital art is a new form of creating art based on the concept of non-fungible tokens (NFT). It refers to a token in form of a digital asset, which is not fungible, thus authentic and unique. The technology is based on various blockchains that solve forgery issues and transparency in a piece's lifeline.  <br />
          <br />Furthermore, we estimate great potential in authenticating physical artworks that are tied to an NFT. A dual threat of physical and digital is a new way to enhance a piece's value as well as speak to a new group of enthusiasts. <br />
            <br />Some would connotate NFT with being a product of pop culture without any evaluatory basis as masses of products and producers who refer to their work as art flooded the market without any qualitative regulatory.<br />
            <br />Let us adopt a new perspective and view this technology as nothing else but a new medium - digital art. The painter's mediums are canvas and colour. The artist manipulates the plasticity of paint on the canvas, uses its characteristics, brings colours and placement into a symphonic harmony.<br />
            <br />What connects the sculptural artist and the painter is not the medium but the craftsmanship, intent, and intellectual property to express yourself precisely without using the spoken word as we all have been taught. Turning our attention back to today's creative souls whom we must detach from the medium and see them for the power of expression and craftsmanship. </p>
        </div>

        <div className='medium-second-column'>
          <p>A contemporary form of art and curation demands sharpness and resilience from its actors, less unified opinions, and more individual viewpoints ranging from ecstatic to critical. Not many can raise enough support for one's work and creations, especially something not yet qualitatively proven.<br />
          <br />We would describe it as a balance of trying to mirror and lead the zeitgeist to blend it with one's perspective. Sometimes the new might be a strong contrast from what great art was supposed to look like. <br />
          <br />Many will keep questioning its worth as it has not stood the test of time - it could not have. In hindsight, the critique can fuel its most virtuous and prodigious proponents.  An evolution towards the new and uncomfortable, which one might believe is a weighted reason for each period's achievements.<br />
          <br />Contemporarists will always encounter a form of adversity and can overcome it by growing taller than their critics. It reminds us that premature judgment closes our eyes for something awaiting the horizon, which might create unforeseeable value - not only monetarily. A brave mind and seeing opportunity in the up and coming is what defines the pioneer. There is always a point to start from, and you just discovered it.</p>
        </div>
      </div>

    </div>

  )
}

export default Medium