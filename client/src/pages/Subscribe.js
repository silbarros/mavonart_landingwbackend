import React, { useState } from "react";
import '../App.css';
import SubscribeForm from './SubscribeForm'
import '../css/Subscribe.css'
<link href = "https://fonts.googleapis.com/css?family=Roboto+Mono|Roboto+Slab|Roboto:300,400,500,700" rel = "stylesheet" />

function App() {
  return (
    <div className='section subscribe' id='subscribe'>
      <div className='subscribe-content'>
        <h1 className='subscribe-title'>Subscribe</h1>
        <div className='subscribe-content-text'>
          <p>Subscribe to our newsletter and receive the last news about digital art!</p>
          <SubscribeForm/>
        </div>
      </div>
    </div>
  );
}

export default App;
