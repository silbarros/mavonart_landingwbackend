import React, { useEffect, useState } from 'react';
import { NavLink, withRouter } from 'react-router-dom'
import '../App.css';
import * as Scroll from 'react-scroll'


const Footer = () => {

  const Link = Scroll.Link
  const [currentPage, setCurrentPage] = useState('')

  useEffect(()=>{
    // console.log(`hello I changed page`)
    setCurrentPage(window.location.pathname)    
  },[window.location.pathname])

  return (
      <footer className='footer'>
        <div className="footer-wrapper">
          <div className='column-left'>
            <h2>Contact Us</h2>
            <br></br>
            <p><NavLink exact to={'/contact-us'}>Let's Talk</NavLink></p>
            <br></br>
            <p><a href='mailto:info@mavon.art' target='_blank'>info@mavon.art</a></p>
            <br></br>
            <p><a href='http://linkedin.com' target='_blank'>LinkedIn</a></p>
            <br></br>
          </div>

          {/* <div className='column-right'> */}
            <div>
              <h2>Website</h2>
              <br></br>
              {/* {currentPage=='/'?
                <p><a href='#philosophy'>Our Philosophy</a></p>:
                <p><NavLink exact to={'/#philosophy'}>Our Philosophy</NavLink></p>
              } */}

          {currentPage == '/' ?
        <Link 
          activeClass="active"
          to='philosophy'
          smooth={true}
          hashSpy={true}
          duration={500}
          isDynamic={true}
          ignoreCancelEvents={false}
          spyThrottle={500}
        >
         {/* <a href="#home"> */}
         <p>Our Philosophy</p>
         {/* </a> */}
         </Link> :
        <NavLink exact to={'/'}>
          <p>Our Philosophy</p>
        </NavLink>
      }


              <br></br>
              <p><NavLink exact to={'/who-we-serve'}>Who We Serve</NavLink></p>
              <br></br>
              <p><NavLink exact to={'/medium'}>The Medium</NavLink></p>
            </div>

            <div>
              <h2>Legal</h2>
              <br></br>
              <p>Privacy Policy</p>
            </div>
          </div>
      </footer>
  )
};

export default withRouter(Footer);
