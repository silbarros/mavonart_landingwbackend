import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Helmet } from "react-helmet"
// import { HashLink as Link } from 'react-router-hash-link';
import newlogo from '../images/newlogo.png'
import '../App.css';
import '../css/Menu.css'
import * as Scroll from 'react-scroll'


const Header = (props) => {

  const [menuIsOpened, setMenuIsOpened] = useState(false)

  const Link = Scroll.Link
  var currentPage = window.location.pathname;
  // console.log('href===>',window.location.pathname) // returns the absolute URL of a page

  return (
    <div className='header'>
      {/* <Helmet>
        <html lang="en" />
        <title>Mavon | Make Art Accessible</title>
        <meta name="description" content="Join our unique Digital Marketplace for non-fungible tokens (NFTs) to create your own private rooms and exhibit exclusive digital assets." />
        {/* <meta name="robots" content="nosnippet" /> */}
      {/* </Helmet> */}

      {/* <nav role="navigation">
        <div id="menuToggle">
          <p onClick={() => setMenuIsOpened(!menuIsOpened)}>&nbsp;&nbsp;&nbsp;&nbsp;</p>

          {menuIsOpened
            ? <div >

              <ul id="menu" className='menu-grid'>
                <div className='menu-left'>
                  <li>
                    <span onClick={() => setMenuIsOpened(false)}>
                      <h1><NavLink exact to={'/'}>Home</NavLink></h1>
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </li>
                </div>

                <div className='menu-left'>
                  <li>
                    <span onClick={() => setMenuIsOpened(false)}>
                     {window.location.pathname!=='/'?
                      <h1><NavLink to="/#philosophy">Our Philosophy</NavLink></h1>
                      :<h1><a href='#philosophy'>Our Philosophy</a></h1>} 
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </li>
                </div>

                <div className='menu-left'>
                  <li>
                    <span onClick={() => setMenuIsOpened(false)}>
                      <h1><NavLink exact to={'/who-we-serve'}>Who We Serve</NavLink></h1>
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </li>
                </div>

                <div className='menu-left'>
                  <li>
                    <span onClick={() => setMenuIsOpened(false)}>
                      <NavLink exact to={'/medium'}><h1>The Medium</h1></NavLink>
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  </li>
                </div>

                <div className='vertical-line'></div>

                <div className='menu-right'>
                  <img src={newlogo} alt='Mavon'></img>
                  <div className='p-wrapper'><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p></div>
                </div>

                <div className='close-button'>
                  <h2 onClick={() => setMenuIsOpened(false)} >✕</h2>
                </div>
              </ul>
            </div>
            : null
          }

        </div>
      </nav> */}

      {currentPage == '/' ?
        <Link 
        activeClass="active"
          to='home'
          smooth={true}
          hashSpy={true}
          duration={500}
          isDynamic={true}
          ignoreCancelEvents={false}
          spyThrottle={500}
        >
         {/* <a href="#home"> */}
          <img src={newlogo} alt='Mavon' className='logo'></img>
         {/* </a> */}
         </Link> :
        <NavLink exact to={'/'}>
          <img src={newlogo} alt='Mavon' className='logo'></img>
        </NavLink>
      }

      <NavLink exact to={'/contact-us'} className='contact-us-button'>contact&nbsp;us</NavLink>
      <div className='space-for-mobile-ver'>&nbsp;</div>

    </div>
  )
}

export default Header