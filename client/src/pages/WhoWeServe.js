import React from 'react'
import { NavLink } from 'react-router-dom'
import {Helmet} from "react-helmet"
import '../css/WhoWeServe.css'

const WhoWeServe = (props) => {

  return <div className='who-we-serve'>
      <Helmet>
        <html lang="en" />
        <title>Mavon | Who We Serve</title>
        <meta name="description" content="We present you, the gallerist, the opportunity to introduce a digital showroom. This room serves as an extension of your physical space to explore the tokenized art world." />
        {/* <meta name="robots" content="nosnippet"/> */}
      </Helmet>

      <p className='who-we-serve-button'><NavLink exact to={'/'}>← Go Back Home</NavLink></p>
      
      <div className='who-we-serve-section'>
      <h1 className='gallerist-title'>The Gallerist</h1>

      <h2 className='who-we-serve-subtitle'>“Our eyes for the valuable.”</h2>
      <div className='who-we-serve-columns'>
        <div>
          <p>We present you, the gallerist, the opportunity to introduce a digital showroom. This room serves as an extension of your physical space to explore the tokenized art world. A three-dimensional setting that we build to highlight and accentuate your gallery's curation. All artworks are exhibited, promoted, and sold through your independent showroom and perspective's augmentation. </p>
        </div>
        <div>
          <p> We provide the essential infrastructure and manifest a solid foundation. It is then your floor to dance on. Notably, the consolidation of a network and user base offers the highest degree of attention for participating galleries. In order to maximize efficiency our data assists you in extracting the most value for your business. So tie up your finest shoes and start dancing!</p>
          <br></br><br></br>
          <p id="p1">Connect.</p> 
        </div>
      </div>
    </div>


    <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1 }} />

    <div className='who-we-serve-section'>
      <h1 className='collector-title'>The Collector</h1>
      <h2 className='who-we-serve-subtitle'>“Our shared emotion for the valuable.”</h2>
      <div className='who-we-serve-columns'>
        <div>
          <p>You, as a collector, are now able to explore digital art close to home. Using all three dimensions to let the artworks unfold their enticing radiance. We strive to enhance the collector‘s experience above and beyond. A chance that all of us can take to create a new world of taste for digital art. The interaction between the tastemakers is very much to be encouraged. Therefore you are free to share your perspectives and collection.
            <br></br><br></br>
            Certainly, you can acquire and sell your artworks as well as securely store the assets. Yet, it is about the asset as an art piece that you want personal access to as well as publicise the richness of thought or beauty. Finding the right balance between idea, beauty, technique, story, or whatever draws you to an artwork is essentially fueled by critical observations and discussions.</p>
        </div>
        <div>
          <p> Oftentimes the patronage of collectors and gallerist shape each other’s focus and is genuinely intricate. We strongly believe in the gallerist’s sensitivity to reduce digital art to qualitative aspects that you cherish most about your physical collection and arrangements. It is about an extension of your collector’s soul to an evolved contemporary state.</p> 
            <br></br><br></br>
            <p id="p2">Collect.</p>
        </div>
      </div>
    </div>





    <hr style={{ backgroundColor: "#cccccc", border: "none", height: 1 }} />

    <div className='who-we-serve-section who-we-serve-artist-wrapper'>
      <h1 className='artist-title'>The Artist</h1>
      <h2 className='who-we-serve-subtitle'>“Our creators of the valuable.”</h2>
      <div className='who-we-serve-columns'>
        <div>
          <p>We are building a space not only to create a storage or marketplace but to enhance the perception of your work. A customisable room provides the possibility of authenticating the experience into the slightest details without any worldly restrictions. 
            <br></br><br></br>
            The platform facilitates the artist's way into important collections while maintaining the product's visibility and supporting your brand. After all, you are the realizers of what is promoted by the gallerist and collected by the enthusiast. The network of galleries is a crucial way of positioning oneself as a qualitatively recognised artist. The gallery's credibility often opens the collectors’ eyes to the worthy and gifted - as a door opener and protectionist. </p>
        </div>
        <div>
          <p>As the landscape of digital artists is blurring borders, we support the idea of reducing the number of participants to a reasonable point. In other words, giving full attention to those who have earned it. Paired with an open-minded attitude and never shutting down, we give room to an exciting group of artists that shall prove themselves over the next decade and beyond.
            <br></br><br></br>
            Whether you are rooted in more traditional artforms or natively digital, the creativity of thought and expression is who and what we are giving a space to. Any artist is free to expand their repertoire, which might polish and refine their artistic language. Or even trying to test your ideas in different environments could enrich you and your broadening audience.</p>
            <br></br><br></br>
            <p id="p2">Create.</p>
        </div>
      </div>
    </div>

  </div>
}

export default WhoWeServe