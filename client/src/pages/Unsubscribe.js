import axios from 'axios'
import React, {useState} from 'react'
import { URL } from '../config';


const Unsubscribe =()=> {

  const [whoIsUnsubscribing, setWhoIsUnsubscribing] = useState('collector')
  console.log('whoIsUnsubscribing==>', whoIsUnsubscribing)

  const [email, setEmail] = useState('')
  console.log('email===>', email)

  const [message, setMessage] = useState('')
  console.log('message===>', message)

  const handleEmail =(event)=> {
    setEmail(event.target.value)
  }


  const handleUnsubscribe =async(event)=> {
    event.preventDefault();
    try {
      // const response = await axios.post(`${URL}/subscribe-${whoIsUnsubscribing}/delete`, {
      const response = await axios.post(`${URL}/unsubscribe-user`, {
        email: email
      })
      setMessage(response.data.message)
      setTimeout(() => {
        setMessage('')
      }, 3000);
      event.target.reset()
    } catch (error) {
      console.log('error===>', error)
    }
  }
  
  return <div style={{height: '50vh'}}>
    <h1>Unsubscribe</h1>
    <h2>If you wish to unsubscribe, enter your email here</h2>

  <form onSubmit={handleUnsubscribe}>
    <input className='' name='email' placeholder='your email' onChange={handleEmail}/>

    <br></br><br></br>

    {/* I am a..
    <div>
        <input
          type='radio'
          onChange={e => setWhoIsUnsubscribing('collector')}
          name='whoIsUnsubscribing'
          defaultChecked
        />
        <label>Collector</label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input
          type='radio'
          onChange={e => setWhoIsUnsubscribing('gallerist')}
          name='whoIsUnsubscribing'
        />
        <label>Gallerist</label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input
          type='radio'
          onChange={e => setWhoIsUnsubscribing('artist')}
          name='whoIsUnsubscribing'
        />
        <label>Artist</label>
      </div> */}

    <br></br>

    <button className=''>Unubscribe</button>
  </form>

  <p>{message}</p>

  </div>
}

export default Unsubscribe;