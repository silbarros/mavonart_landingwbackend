// // const URL = `http://localhost:3001`;
// import axios from 'axios'
// // =======  preparing to the deplyment  ========
// const URL = window.location.hostname === `localhost`
//             ? `http://localhost:3001` // 3030 should be replaced with your server port
//             : `https://silbarros.gitlab.io/mavonart_landingwbackend/` // it should be replaced with actual domain during the deployment
// // =============================================
// const customInstance = axios.create ({
//   baseURL : URL, 
//   headers: {'Accept': 'application/json'}
// })

// export default customInstance;

// export { URL };

// ========== For deploy in Digital Ocean ==========
import axios from 'axios'
// const URL = `http://localhost:3001`;

// =======  preparing to the deplyment  ========
const URL = window.location.hostname === `localhost`
            // ? `http://localhost:3040` // For deployed website. 3030 should be replaced with your server port
            // ? `http://localhost:3001` // For testing website
            ? `https://mavon.art:8443` // For testing website
            : `https://54.175.246.133/` // it should be replaced with actual domain during the deployment
// =============================================
const customInstance = axios.create ({
  baseURL : URL, 
  headers: {'Accept': 'application/json'}
})

export default customInstance;

export { URL };
